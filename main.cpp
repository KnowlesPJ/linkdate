#include <chrono>
#include <ctime>
#include <filesystem>
#include <iostream>

int main(int argc, char* argv[]) {
  namespace fs = std::filesystem;
  auto executable = fs::path{argv[0]};
  auto last_write_time = fs::last_write_time(executable);
  auto cftime = std::chrono::system_clock::to_time_t(std::chrono::time_point_cast<std::chrono::system_clock::duration>(
      last_write_time - decltype(last_write_time)::clock::now() + std::chrono::system_clock::now()));
  std::cout << executable << " last written on " << std::asctime(std::localtime(&cftime)) << std::endl;
  return 0;
}
